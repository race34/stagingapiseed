// Sidebar Routers
export const category = [
	{
		action: '',
		title: 'invoices',
		items: null,
		path: '/invoices'
	},
	{
		action: '',
		title: 'products',
		items: null,
		path: '/home/products'
	},
	{
		action: '',
		title: 'productsVariation',
		items: null,
		path: '/productions-variation'
	},
	{
		action: '',
		title: 'profiles',
		items: null,
		path: '/profiles'
	},
	{
		action: '',
		title: 'shipments',
		items: null,
		path: '/shipments'
	},
	{
		action: '',
		title: 'userRoles',
		items: null,
		path: '/user-roles'
	},
	{
		action: '',
		title: 'woocommerceProducts',
		items: null,
		path: '/wc'
	},
	{
		action: '',
		title: 'woocommerceSite',
		items: null,
		path: '/woocommerce-site'
	},
	{
		action: '',
		title: 'woocommerceSyncLogs',
		items: null,
		path: '/woocommerce-sync-logs'
	},
	{
		action: '',
		title: 'temporaraylogout',
		items: null,
		path: '/logout'
	}
]

