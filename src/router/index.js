import Vue from 'vue'
import Router from 'vue-router'

// layout components
import Full from '@/container/Full'

// dashboard components
import DashboardOne from '@/views/dashboard/DashboardOne'
import LoginForm from '@/views/login/form'

Vue.use(Router)

export default new Router({
	routes: [
		{
		path: '/',
		component: require('../views/login/form.vue').default
		},
		{
			path: '/logout',
			component: require('../views/login/logout.vue').default
			},
		{
			path: '/home',
			component: Full,
			redirect: '/dashboard/dashboard-v1',
			children: [
				{
					path: '/dashboard/dashboard-v1',
					component: DashboardOne,
					meta: {
						title: 'Dashboard',
						breadcrumb: 'Dashboard / Dashboard'
					}
				},
				{
					path: '/home/products',
					component: require('../views/products/list.vue').default,
					meta: {
						title: 'products',
						breadcrumb: 'home / products'
					}
				}
			]
    },
    {
      path: '/wc',
      component: Full,
      children: [
        {
          path: '/',
          component: require('../views/wc/listWooProducts.vue').default,
          meta : {
            title: 'Woo Products',
            breadcrumb: 'wc/ List Woo Products'
          }
        },
        {
          path: '/woocommerce-sync-logs',
          component: require('../views/wc/wooSyncLogs.vue').default,
          meta: {
            title: 'Woo Sync Logs',
            breadcrumb: 'wc/ Woo Sync Logs'
          }
        }
      ]
    }
	]
})
