import Vue from '../main.js'
import Router from '../router/index.js'
import Axios from 'axios'
import qs from 'qs'

export default({
	login(context){

		var querystring = require('querystring')

		if(context.$session.get('token')){
			window.location.href = "http://localhost:8080/#/home"
		} else {
			Axios.post('/login2.json', qs.stringify(context.loginData),{headers: {'Content-Type':'application/x-www-form-urlencoded'}}).then(
				(response) => {
          context.$session.start()
					context.$session.remove('token')
          context.$session.set('token', response.data.token)

          Axios.get('/me.json', {headers: {'Content-Type' : 'application/json', 'Authorization' : context.$session.get('token')}}).then(
            (response) => {
                context.$session.set('user', response.data.user)
            }
          ).catch((response) => { console.log(response) })

					context.initialize()
				},
				(response) => {
					alert(response)
					context.notification = "Invalid Authentication"
					context.notificationSnackbar = true
					console.log(response)
				}
			).catch((response => { console.log(response) }))
		}
	},
	logout(){
		this.$session.remove('token')
		this.$session.remove('user')
		Router.push('/')
	}
})
