/* eslint-disable */
//messages.js
module.exports = {
	home: 'Home',
	dashboard: 'Dashboard',
	invoices: 'Invoices',
	products: 'Products',
	productsVariation: 'Product Variations',
	profiles: 'Profiles',
	shops: 'Shops',
	shipments: 'Shipments',
	userRoles: 'User Roles',
	woocommerceProducts: 'wc Products',
	woocommerceSite: 'wc Site',
	woocommerceSyncLogs: 'wc Sync Logs',

	//title divider
	general: 'General'
}
